import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GamesListComponent } from './games-list/games-list.component'
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MatchsListComponent } from './matchs-list/matchs-list.component';
import { PhaseDetailsComponent } from './phase-details/phase-details.component';
import { PlayerProfileComponent } from './player-profile/player-profile.component';
import { PlayersListComponent } from './players-list/players-list.component';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { TeamsListComponent } from './teams-list/teams-list.component';
import { TournamentDetailsComponent } from './tournament-details/tournament-details.component';
import { TournamentsListComponent } from './tournaments-list/tournaments-list.component';
const routes: Routes = [
  { path: '',   redirectTo: '/login', pathMatch: 'full' },
  {path: 'home', component: HomeComponent, children: [
    {path: 'games', component: GamesListComponent},
    {path: 'tournaments', component: TournamentsListComponent},
    {path: 'tournaments/:tournamentId', component: TournamentDetailsComponent},
    {path: 'tournaments/:tournamentId/phase/:phaseId', component: PhaseDetailsComponent},
    {path: 'matchs', component: MatchsListComponent},
    {path: 'players/me', component: PlayerProfileComponent},
    {path: 'players', component: PlayersListComponent},
    {path: 'players/:userId', component: PlayerProfileComponent},
    {path: 'teams', component: TeamsListComponent},
    {path: 'teams/:teamId', component: TeamDetailsComponent},
  ]},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}