import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ApiModule } from 'build/openapi/api.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field'
import {  MatGridListModule } from '@angular/material/grid-list';
import { GamesListComponent } from './games-list/games-list.component'
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms'
import { Configuration } from 'build/openapi';
import { environment } from 'src/environments/environment';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatchsListComponent } from './matchs-list/matchs-list.component';
import { PlayersListComponent } from './players-list/players-list.component';
import { PhasesListComponent } from './phases-list/phases-list.component';
import { RegistrationsListComponent } from './registrations-list/registrations-list.component';
import { TeamRegistrationsListComponent } from './team-registrations-list/team-registrations-list.component';
import { TournamentTeamListComponent } from './tournament-team-list/tournament-team-list.component';
import { ParticipantListComponent } from './participant-list/participant-list.component';
import { OrganizersListComponent } from './organizers-list/organizers-list.component';
import { PlayerProfileComponent } from './player-profile/player-profile.component';
import { TournamentsListComponent } from './tournaments-list/tournaments-list.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TournamentDetailsComponent } from './tournament-details/tournament-details.component';
import { TeamsListComponent } from './teams-list/teams-list.component';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { PhaseDetailsComponent } from './phase-details/phase-details.component';
import { HomeComponent } from './home/home.component'
import { MatInputModule } from '@angular/material/input';
import { CreatePlayerRegistrationComponent } from './create-player-registration/create-player-registration.component';
import { CreateTeamRegistrationComponent } from './create-team-registration/create-team-registration.component';
import { CreateTournamentComponent } from './create-tournament/create-tournament.component';
import { CreateTeamComponent } from './create-team/create-team.component';
import { CreatePhaseComponent } from './create-phase/create-phase.component';
import { CreateMatchComponent } from './create-match/create-match.component'
import { MatDialogModule } from '@angular/material/dialog'
import { MatDatepickerModule  } from '@angular/material/datepicker'
import { MatNativeDateModule, MAT_DATE_FORMATS } from '@angular/material/core'
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import { MatSelectModule } from '@angular/material/select';
import { TeamSelectComponentComponent } from './team-select-component/team-select-component.component'

const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: ' YYYY-MM-DD',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DD.MM.YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};

@NgModule({
  declarations: [
    AppComponent,
    GamesListComponent,
    LoginComponent,
    NavigationComponent,
    MatchsListComponent,
    PlayersListComponent,
    PhasesListComponent,
    RegistrationsListComponent,
    TeamRegistrationsListComponent,
    TournamentTeamListComponent,
    ParticipantListComponent,
    OrganizersListComponent,
    PlayerProfileComponent,
    TournamentsListComponent,
    TournamentDetailsComponent,
    TeamsListComponent,
    TeamDetailsComponent,
    PhaseDetailsComponent,
    HomeComponent,
    CreatePlayerRegistrationComponent,
    CreateTeamRegistrationComponent,
    CreateTournamentComponent,
    CreateTeamComponent,
    CreatePhaseComponent,
    CreateMatchComponent,
    TeamSelectComponentComponent,
  ],
  imports: [
    MatSelectModule,
    MatMomentDateModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ApiModule.forRoot(() => {
      return new Configuration({
        basePath: `${environment.HOST}:${environment.PORT}`,
      });
    }),
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    FlexLayoutModule,
    MatGridListModule,
    MatCardModule,
    MatFormFieldModule
  ],
  providers: [{ provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }],
  bootstrap: [AppComponent]
})
export class AppModule { }
