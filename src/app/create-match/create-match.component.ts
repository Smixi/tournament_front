import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Game, GamesService, Match, MatchInput, MatchsService, TournamentTeam, TournamentTeamsService } from 'build/openapi';
import { CreatePhaseComponent } from '../create-phase/create-phase.component';

@Component({
  selector: 'app-create-match',
  templateUrl: './create-match.component.html',
  styleUrls: ['./create-match.component.scss']
})
export class CreateMatchComponent implements OnInit {

  games: Game[] = []
  tournamentTeams: TournamentTeam[] = []

  form = this.formBuilder.group({
    game: '',
    team1: '',
    team2: '',
    startDate: ''
  });

  constructor(private formBuilder: FormBuilder, private tournamentTeamsService: TournamentTeamsService, private matchsService: MatchsService,
    @Inject(MAT_DIALOG_DATA) public data: {phaseId: number, tournamentId: number}, public dialogRef: MatDialogRef<CreatePhaseComponent>,
    private gamesService: GamesService) { }

  ngOnInit(): void {
    this.gamesService.gamesList().subscribe(games => {
      this.games = games
    })
    this.tournamentTeamsService.tournamentTeamsList(undefined, this.data.tournamentId).subscribe(tTeams => {
      this.tournamentTeams = tTeams;
    })
  }

  onSubmit(){
    const matchInput: MatchInput = {game:  this.form.value.game, phase: this.data.phaseId, tournamentTeams: [this.form.value.team1, this.form.value.team2], startDate: this.form.value.startDate}
    this.matchsService.matchsCreate(matchInput).subscribe(()=>{
      this.dialogRef.close()
    })
  }

}
