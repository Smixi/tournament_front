import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PhaseInput, PhasesService, TournamentTeam, TournamentTeamsService } from 'build/openapi';

@Component({
  selector: 'app-create-phase',
  templateUrl: './create-phase.component.html',
  styleUrls: ['./create-phase.component.scss']
})
export class CreatePhaseComponent implements OnInit {

  tournamentTeams: TournamentTeam[] = []

  form = this.formBuilder.group({
    phaseName: '',
    tournamentTeams: [],
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: {tournamentId: number}, 
  private phaseService: PhasesService, private tournamentTeamsService: TournamentTeamsService,
  private formBuilder: FormBuilder,
  public dialogRef: MatDialogRef<CreatePhaseComponent>) {
    
  }

  ngOnInit(): void {
    this.tournamentTeamsService.tournamentTeamsList(undefined, this.data.tournamentId).subscribe(tTeams=>{
      this.tournamentTeams = tTeams;
    })
  }

  onSubmit(){
    const phaseInput: PhaseInput = {name: this.form.value.phaseName, tournament: this.data.tournamentId,playingTeams: !!this.form.value.tournamentTeams?this.form.value.tournamentTeams : []}
    this.phaseService.phasesCreate(phaseInput).subscribe(()=>{
      this.dialogRef.close()
    })
  }

}
