import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePlayerRegistrationComponent } from './create-player-registration.component';

describe('CreatePlayerRegistrationComponent', () => {
  let component: CreatePlayerRegistrationComponent;
  let fixture: ComponentFixture<CreatePlayerRegistrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatePlayerRegistrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePlayerRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
