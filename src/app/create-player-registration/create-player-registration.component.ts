import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RegistrationInput, RegistrationsService, TournamentTeam, TournamentTeamsService } from 'build/openapi';
import { UserService } from '../user.service';

@Component({
  selector: 'app-create-player-registration',
  templateUrl: './create-player-registration.component.html',
  styleUrls: ['./create-player-registration.component.scss']
})
export class CreatePlayerRegistrationComponent implements OnInit {

  checkoutForm = this.formBuilder.group({
    tournamentDisplayName: '',
    tournamentTeam: '',
    registrationInfo: '',
  });


  tournamentTeams: TournamentTeam[] = []

  constructor(public dialogRef: MatDialogRef<CreatePlayerRegistrationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {tournamentId: number}, private formBuilder: FormBuilder, 
    private registrationService: RegistrationsService, private userService: UserService,
    private tournamentTeamService: TournamentTeamsService) {
    }

  ngOnInit(): void {
    this.tournamentTeamService.tournamentTeamsList(undefined, this.data.tournamentId).subscribe(tournamentTeams => {
      this.tournamentTeams = tournamentTeams
    })
  }

  onSubmit(){
    const registrationInput: RegistrationInput = { player: this.userService.userId! , 
      tournament: this.data.tournamentId, tournamentDisplayName: this.checkoutForm.value.tournamentDisplayName, 
      tournamentTeam:  this.checkoutForm.value.tournamentTeam, registrationInfo: this.checkoutForm.value.registrationInfo}
    this.registrationService.registrationsCreate(registrationInput).subscribe(()=>this.closeDialog())
  }

  closeDialog() {
    this.dialogRef.close();
  }

}
