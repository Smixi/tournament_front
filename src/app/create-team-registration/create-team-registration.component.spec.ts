import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTeamRegistrationComponent } from './create-team-registration.component';

describe('CreateTeamRegistrationComponent', () => {
  let component: CreateTeamRegistrationComponent;
  let fixture: ComponentFixture<CreateTeamRegistrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateTeamRegistrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTeamRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
