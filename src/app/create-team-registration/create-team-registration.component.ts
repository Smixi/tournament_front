import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RegistrationsService, Team, TeamRegistrationsService, TeamsService, TeamRegistrationInput} from 'build/openapi';
import { UserService } from '../user.service';

@Component({
  selector: 'app-create-team-registration',
  templateUrl: './create-team-registration.component.html',
  styleUrls: ['./create-team-registration.component.scss']
})
export class CreateTeamRegistrationComponent implements OnInit {

  form = this.formBuilder.group({
    teamDisplayName: '',
    registrationInfo: '',
    team: '',
  });

  teams: Team[] = []

  constructor(public dialogRef: MatDialogRef<CreateTeamRegistrationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {tournamentId: number}, private formBuilder: FormBuilder, 
    private teamRegistrationService: TeamRegistrationsService, private userService: UserService,
    private teamService: TeamsService) {
    }

  ngOnInit(): void {
    this.teamService.teamsList().subscribe(teams => {
      this.teams = teams
    })
  }

  onSubmit(){
    const input: TeamRegistrationInput = {tournament : this.data.tournamentId, teamDisplayName: this.form.value.teamDisplayName, registrationInfo: this.form.value.registrationInfo, team: this.form.value.team}
    this.teamRegistrationService.teamRegistrationsCreate(input).subscribe(()=>this.closeDialog())
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
