import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { TournamentInput, TournamentsService } from 'build/openapi';
import { UserService } from '../user.service';
import * as  moment from 'moment'

@Component({
  selector: 'app-create-tournament',
  templateUrl: './create-tournament.component.html',
  styleUrls: ['./create-tournament.component.scss']
})
export class CreateTournamentComponent implements OnInit {

  checkoutForm = this.formBuilder.group({
    name: '',
    location: '',
    description: '',
    startingDate: '',
    endDate: ''
  });

  constructor(public dialogRef: MatDialogRef<CreateTournamentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private formBuilder: FormBuilder, private tournamentService: TournamentsService, private userService: UserService) {

    }

  ngOnInit(): void {
  }

  onSubmit(){
    const input: TournamentInput = {name: this.checkoutForm.value.name, description: this.checkoutForm.value.description,
      endDate: moment(this.checkoutForm.value.endDate).format('YYYY-MM-DDThh:mm'),
      location: this.checkoutForm.value.location, organizers: [Number(this.userService.userId)],
      startDate: moment(this.checkoutForm.value.startingDate).format('YYYY-MM-DDThh:mm')
    }
    this.tournamentService.tournamentsCreate(input).subscribe(()=>{this.closeDialog()})
  }

  closeDialog() {
    this.dialogRef.close();
  }

}
