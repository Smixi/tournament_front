import { Component, Input, OnInit } from '@angular/core';
import { GamesService } from 'build/openapi';
import { Game } from 'build/openapi'

@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.scss']
})
export class GamesListComponent implements OnInit {

  @Input('user-id') userId?: number
  games: Game[] = [];
  columnsToDisplay = ['name', 'genre', 'publishedDate']

  constructor(private gameService: GamesService) { }

  ngOnInit(): void {
    if (!this.userId){
      this.gameService.gamesList().subscribe(games => {
        this.games = games
      })
    }
  }

  ngOnChanges(changes: any){
    const userId: number | undefined = changes.userId?.currentValue;
    if( !!userId ){
      this.gameService.gamesList(undefined, undefined, undefined, userId).subscribe(games => {
        this.games = games
      })
    }
  }

}
