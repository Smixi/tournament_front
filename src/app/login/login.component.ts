import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'build/openapi/api/auth.service'
import { TournamentsService } from 'build/openapi/api/tournaments.service'
import { Configuration } from 'build/openapi/configuration'
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string = "";
  password: string = "";

  constructor(private authService: AuthService, private userService: UserService,
   private apiConfiguration: Configuration  , private router: Router) {}

  ngOnInit(): void {
  }

  /**
   * Login the user using username and password. Get a jwt token an store it locally.
   */
  public login(){
    this.userService.login(this.username, this.password);
  }

}
