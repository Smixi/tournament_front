import { Component, Input, OnInit } from '@angular/core';
import { Match, MatchsService } from 'build/openapi';

@Component({
  selector: 'app-matchs-list',
  templateUrl: './matchs-list.component.html',
  styleUrls: ['./matchs-list.component.scss']
})
export class MatchsListComponent implements OnInit {

  @Input('phase-id') phaseId?: number;
  matchs: Match[] = [];
  columnsToDisplay = ['startDate', 'endDate', 'progress', 'game', 'teams', 'result']
  constructor(private matchService: MatchsService) { }

  ngOnInit(): void {
    if(!this.phaseId){
      this.matchService.matchsList().subscribe(matchs => {
        this.matchs = matchs
      })
    }
  }

  ngOnChanges(changes: any){
    const phaseId: number | undefined = changes.phaseId?.currentValue;
    if(!!phaseId){
      this.matchService.matchsList(undefined, undefined, undefined, phaseId).subscribe(matchs => {
        this.matchs = matchs
      })
    }
  }

  getTeamNames(match: Match){
    return match.tournamentTeams.map(t => t.registeredTeam.teamDisplayName).join(' VS ')
  }

  // Over simplified results
  getResult(match: Match){
    const result = match.result 
    if (!!result){
      return `${result[match.tournamentTeams[0]?.registeredTeam.id]} - ${result[match.tournamentTeams[1]?.registeredTeam.id]}`
    }
    else {
      return ""
    }
  }
}
