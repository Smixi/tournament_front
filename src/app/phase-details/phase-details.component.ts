import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { PatchedPhase, Phase, PhasesService, PhaseTeamPlaying, PhaseTeamQualified } from 'build/openapi';
import { CreateMatchComponent } from '../create-match/create-match.component';
import { TeamSelectComponentComponent } from '../team-select-component/team-select-component.component';

@Component({
  selector: 'app-phase-details',
  templateUrl: './phase-details.component.html',
  styleUrls: ['./phase-details.component.scss']
})
export class PhaseDetailsComponent implements OnInit {

  phase?: Phase
  constructor(private phasesService: PhasesService, private activedRoute: ActivatedRoute, public dialog: MatDialog) { }

  ngOnInit(): void {
    const phaseId = this.activedRoute.snapshot.paramMap.get('phaseId');
    this.phasesService.phasesRetrieve(Number(phaseId)).subscribe(phase => {
      this.phase = phase;
    })
  }

  createMatch(){
    let dialog = this.dialog.open(CreateMatchComponent, {data: {tournamentId: this.phase?.tournament, phaseId: this.phase?.id}})
  }

  modPlaying(){
    let dialog = this.dialog.open(TeamSelectComponentComponent, {data: {tournamentId: this.phase?.tournament, phaseId: this.phase?.id}}).afterClosed().subscribe(result => {
      if (!!result.tournamentTeams){
        const phaseP : PhaseTeamPlaying = {playingTeams: result.tournamentTeams}
        this.phasesService.phasesAddTeamPlayingCreate(this.phase!.id, undefined, phaseP).subscribe()
      }
    })
  }

  modQualified(){
   
    let dialog = this.dialog.open(TeamSelectComponentComponent, {data: {tournamentId: this.phase?.tournament, phaseId: this.phase?.id}}).afterClosed().subscribe(result => {
      if (result.tournamentTeams){
      const phaseQ : PhaseTeamQualified = {qualifiedTeams: result.tournamentTeams}
      this.phasesService.phasesAddTeamPlayingQualifiedCreate(this.phase!.id, undefined, phaseQ).subscribe()
      }
    })
  }
}
