import { Component, OnInit, Input, SimpleChange } from '@angular/core';
import { Phase, PhasesService } from 'build/openapi';

@Component({
  selector: 'app-phases-list',
  templateUrl: './phases-list.component.html',
  styleUrls: ['./phases-list.component.scss']
})
export class PhasesListComponent implements OnInit {

  @Input('tournament-id') tournamentId?: number

  phases: Phase[] = []
  columnsToDisplay = ['name', 'matchs', 'playingTeams', 'qualifiedTeams', 'progress', 'details']
  constructor(private phasesService: PhasesService) {}

  ngOnInit(): void {
  }

  ngOnChanges(changes: any){
    const tournamentId: number = changes.tournamentId.currentValue
    if (!!tournamentId) {
      this.phasesService.phasesList(undefined, undefined, tournamentId).subscribe(phases => {
        this.phases = phases
      })
    }
  }

}
