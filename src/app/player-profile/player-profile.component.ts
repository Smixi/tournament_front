import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Player, PlayersService } from 'build/openapi';
import { FormBuilder } from '@angular/forms';
import { UserService } from '../user.service';
@Component({
  selector: 'app-player-profile',
  templateUrl: './player-profile.component.html',
  styleUrls: ['./player-profile.component.scss']
})
export class PlayerProfileComponent implements OnInit {
  
  playerForm = this.formBuilder.group({
    nickname: '',
    games: []
  });

  isSelfProfile: boolean = false;
  player?: Player;
  constructor(private activedRoute: ActivatedRoute, private playersService: PlayersService,
    private formBuilder: FormBuilder, private userService: UserService) { }

  ngOnInit(): void {
    const id = this.activedRoute.snapshot.paramMap.get('userId');
    this.isSelfProfile = this.userService.userId == Number(id) ? true : false
    if (id != null) {
      if (id === 'me') {
        this.isSelfProfile = true;
        const id = this.userService.userId;
        this.playersService.playersRetrieve(Number(id)).subscribe(player => {
          this.player = player;
          this.playerForm.patchValue({'nickname': player.nickname, 'games': player.games})
        })
      }
      else {
        this.playersService.playersRetrieve(Number(id)).subscribe(player => {
          this.player = player;
          this.playerForm.patchValue({'nickname': player.nickname, 'games': player.games})
        })
      }
    }
  }

  onSubmit(): void {
    this.playersService.playersUpdate(this.player!.user, this.playerForm.value.player, this.playerForm.value.games).subscribe(updatedPlayer => {
      this.player= updatedPlayer;
    })
  }

}
