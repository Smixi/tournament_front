import { Component, OnInit } from '@angular/core';
import { Player, PlayersService } from 'build/openapi';

@Component({
  selector: 'app-players-list',
  templateUrl: './players-list.component.html',
  styleUrls: ['./players-list.component.scss']
})
export class PlayersListComponent implements OnInit {

  players: Player[] = [];
  columnsToDisplay = ['nickname', 'teamsNumber', 'favoriteGames', 'profile']
  constructor(private playerService: PlayersService) { }

  ngOnInit(): void {
    this.playerService.playersList().subscribe(players => {
      this.players = players
    })
  }

}
