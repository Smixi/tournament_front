import { Component, Input, OnInit } from '@angular/core';
import { Registration, RegistrationsService } from 'build/openapi';

@Component({
  selector: 'app-registrations-list',
  templateUrl: './registrations-list.component.html',
  styleUrls: ['./registrations-list.component.scss']
})
export class RegistrationsListComponent implements OnInit {
  
  @Input('tournament-id') tournamentId?: number
  @Input('user-id') userId?: number
  @Input('hide-actions') hideActions: boolean = false

  registrations: Registration[] = [];

  get columnsToDisplay(){
    if (this.hideActions){
      return ['player', 'tournamentDisplayName', 'tournamentTeam', 'registrationDate', 'validated']
    }
    return ['player', 'tournamentDisplayName', 'tournamentTeam', 'registrationDate', 'validated', 'action']
  }

  constructor(private registrationsService: RegistrationsService) {}

  ngOnInit(): void {

  }

  ngOnChanges(changes: any){
    const tournamentId: number | undefined = changes.tournamentId?.currentValue;
    const userId: number | undefined = changes.userId?.currentValue;
    if (!!userId || !!tournamentId) {
      this.registrationsService.registrationsList(undefined, userId, tournamentId).subscribe(registrations => {
        this.registrations = registrations;
      })
    }
  }

  validateRegistration(registration : Registration){
    this.registrationsService.registrationsValidateCreate(registration.id).subscribe(validatedRegistration => {
      registration = validatedRegistration
    })
  }
  
  closeRegistration(registration : Registration){
    this.registrationsService.registrationsDestroy(registration.id).subscribe(deleted=>{
      this.registrations.splice(this.registrations.findIndex(reg => { return registration.id == reg.id}))
    })
  }

}
