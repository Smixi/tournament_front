import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamRegistrationsListComponent } from './team-registrations-list.component';

describe('TeamRegistrationsListComponent', () => {
  let component: TeamRegistrationsListComponent;
  let fixture: ComponentFixture<TeamRegistrationsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamRegistrationsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamRegistrationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
