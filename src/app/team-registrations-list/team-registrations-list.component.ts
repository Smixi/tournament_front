import { Component, Input, OnInit } from '@angular/core';
import { TeamRegistration, TeamRegistrationsService } from 'build/openapi';

@Component({
  selector: 'app-team-registrations-list',
  templateUrl: './team-registrations-list.component.html',
  styleUrls: ['./team-registrations-list.component.scss']
})
export class TeamRegistrationsListComponent implements OnInit {
  
  @Input('tournament-id') tournamentId?: number

  teamRegistrations: TeamRegistration[] = []
  columnsToDisplay = ['teamDisplayName', 'registrationDate', 'team', 'validated', 'action']
  constructor(private teamRegistrationsService: TeamRegistrationsService) {}

  ngOnInit(): void {

  }

  ngOnChanges(changes: any){
    const tournamentId: number = changes.tournamentId.currentValue;
    if (!!tournamentId) {
      this.teamRegistrationsService.teamRegistrationsList(undefined, tournamentId).subscribe(teamRegistrations => {
        this.teamRegistrations = teamRegistrations;
      })
    }
  }
  
  validateTeamRegistration(teamRegistration : TeamRegistration){
    this.teamRegistrationsService.teamRegistrationsValidateCreate(teamRegistration.id).subscribe(validatedTeamRegistration => {
      teamRegistration = validatedTeamRegistration
    })
  }
  
  closeTeamRegistration(teamRegistration : TeamRegistration){
    this.teamRegistrationsService.teamRegistrationsDestroy(teamRegistration.id).subscribe(deleted=>{
      this.teamRegistrations.splice(this.teamRegistrations.findIndex(reg => { return teamRegistration.id == reg.id}))
    })
  }

}
