import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamSelectComponentComponent } from './team-select-component.component';

describe('TeamSelectComponentComponent', () => {
  let component: TeamSelectComponentComponent;
  let fixture: ComponentFixture<TeamSelectComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamSelectComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamSelectComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
