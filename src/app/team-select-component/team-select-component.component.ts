import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PhasesService, TournamentTeam, TournamentTeamsService } from 'build/openapi';
import { CreatePhaseComponent } from '../create-phase/create-phase.component';

@Component({
  selector: 'app-team-select-component',
  templateUrl: './team-select-component.component.html',
  styleUrls: ['./team-select-component.component.scss']
})
export class TeamSelectComponentComponent implements OnInit {

  tournamentTeams: TournamentTeam[] = []

  form = this.formBuilder.group({
    tournamentTeams: [],
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: {tournamentId: number}, 
  private tournamentTeamsService: TournamentTeamsService,
  private formBuilder: FormBuilder,
  public dialogRef: MatDialogRef<CreatePhaseComponent>) {
    
  }

  ngOnInit(): void {
    this.tournamentTeamsService.tournamentTeamsList(undefined, this.data.tournamentId).subscribe(tTeams=>{
      this.tournamentTeams = tTeams;
    })
  }

  onSubmit(){
    this.dialogRef.close({
      tournamentTeams: this.form.value.tournamentTeams
    })
  }
}
