import { Component, Input, OnInit } from '@angular/core';
import { Team, TeamsService } from 'build/openapi';

@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.scss']
})
export class TeamsListComponent implements OnInit {

  @Input('user-id') userId?: number;

  teams: Team[] = []
  columnsToDisplay = ['name', 'members', 'details']
  constructor(private teamsService: TeamsService) {}

  ngOnInit(): void {
    if (!this.userId) {
      this.teamsService.teamsList(undefined, undefined, undefined).subscribe(teams => {
        this.teams = teams
      })
    }
  }

  ngOnChanges(changes: any){
    const userId: number | undefined = changes.userId?.currentValue;
    const players = userId ? [userId] : undefined
    if(!!players){
      this.teamsService.teamsList(undefined, undefined, players).subscribe(teams => {
        this.teams = teams
      });
    }

  }

}
