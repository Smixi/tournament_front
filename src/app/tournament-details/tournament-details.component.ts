import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { TournamentDetails, TournamentsService } from 'build/openapi';
import { CreatePhaseComponent } from '../create-phase/create-phase.component';
import { CreatePlayerRegistrationComponent } from '../create-player-registration/create-player-registration.component';
import { CreateTeamRegistrationComponent } from '../create-team-registration/create-team-registration.component';

@Component({
  selector: 'app-tournament-details',
  templateUrl: './tournament-details.component.html',
  styleUrls: ['./tournament-details.component.scss']
})
export class TournamentDetailsComponent implements OnInit {

  tournament?: TournamentDetails;
  constructor(private activedRoute: ActivatedRoute, private tournamentsService: TournamentsService,
    public dialog: MatDialog
    ) { }

  ngOnInit(): void {
    const id = this.activedRoute.snapshot.paramMap.get('tournamentId');
    this.tournamentsService.tournamentsRetrieve(Number(id)).subscribe(tournament => {
      this.tournament = tournament
    })
  }

  selfRegister(){
    this.dialog.open(CreatePlayerRegistrationComponent, {data: {tournamentId: this.tournament?.id}})
  }

  registerTeam(){
    this.dialog.open(CreateTeamRegistrationComponent,  {data: {tournamentId: this.tournament?.id}})
  }

  createPhase(){
    this.dialog.open(CreatePhaseComponent,  {data: {tournamentId: this.tournament?.id}})
  }

}
