import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TournamentTeam } from 'build/openapi';

@Component({
  selector: 'app-tournament-team-list',
  templateUrl: './tournament-team-list.component.html',
  styleUrls: ['./tournament-team-list.component.scss']
})
export class TournamentTeamListComponent implements OnInit {

  tournamentTeams: TournamentTeam[] = []
  columnsToDisplay = ['tournamentTeamName', 'tTeamProfile']
  @Input('tournament-teams') inTournamentTeams?: TournamentTeam[];

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: any){
    const tournamentTeams: TournamentTeam[] = changes.inTournamentTeams.currentValue;
    if (!!tournamentTeams && tournamentTeams.length > 0) {
      this.tournamentTeams = tournamentTeams
      tournamentTeams[0].registeredTeam.teamDisplayName
    }
  }

}
