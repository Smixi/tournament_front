import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Tournament, TournamentsService } from 'build/openapi';
import { CreateTournamentComponent } from '../create-tournament/create-tournament.component';

@Component({
  selector: 'app-tournaments-list',
  templateUrl: './tournaments-list.component.html',
  styleUrls: ['./tournaments-list.component.scss']
})
export class TournamentsListComponent implements OnInit {

  tournaments: Tournament[] = []
  columnsToDisplay = ['name', 'startDate', 'endDate', 'location', 'details']
  constructor(private tournamentsService: TournamentsService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.tournamentsService.tournamentsList().subscribe(tournaments => {
      this.tournaments = tournaments
    })
  }
  openCreateDialog(){
    let dialogRef = this.dialog.open(CreateTournamentComponent);
  }
}
