import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, Configuration } from 'build/openapi';
import jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private apiConfiguration: Configuration, private router: Router, private authService: AuthService) {
    const accessToken = localStorage.getItem('jwtAuth')
    if (!!accessToken){
      this.apiConfiguration.credentials['jwtAuth'] = accessToken;
      this.router.navigate(['/home']);
      this.accessToken = accessToken;
      const decodedToken: any = jwt_decode(accessToken);
      this.userId = decodedToken.user_id;
      this.username = localStorage.getItem('username')!
    }
   }

  username: string = "";
  userId?: number;
  accessToken?: string;

  login(username: string, password: string){
    this.authService.authJwtCreateCreate({username, password} as any).subscribe(token => {
      this.apiConfiguration.credentials['jwtAuth'] = token.access;
      localStorage.setItem('jwtAuth', token.access);
      this.router.navigate(['/home']);
      const decodedToken: any = jwt_decode(token.access);
      this.userId = decodedToken.user_id;
      this.username = username;
      localStorage.setItem("username", username);
    })
  }

  /**
   * Remove Authentication data and clear jwt token in local storage
   */
  public logout(){
    this.apiConfiguration.credentials['jwtAuth'] = "";
    localStorage.removeItem('jwtAuth');
    this.router.navigate(['/login'])
  }

}
